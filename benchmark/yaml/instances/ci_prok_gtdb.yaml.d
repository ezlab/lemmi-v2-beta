instance_seed: 1
negative:
    - '001'
calibration:
    - '001'
evaluation:
    - '001'
host_ratio: 0.01
prok_ratio: 0.79
vir_ratio: 0.199
euk_ratio: 0.001
targets:
    - prok
contaminants:
    - euk
    - vir
use_prokncbi: 0
targets_taxonomy: gtdb
contaminants_taxonomy: ncbi
sd_lognormal_vir: 1
sd_lognormal_euk: 1
sd_lognormal_prok: 1
prok_nb: 1
euk_nb: 1
vir_nb: 1
unknown_organisms_ratio: 0
prok_taxa:
    - Acetobacter
euk_taxa:
vir_taxa:
core_simul: 0
tech: 'HS25'
read_length: 150
# read will be assigned depending on ratio above
total_reads_nb: 500
fragment_mean_size: 300
fragment_sd: 25
max_base_quality: 35
evaluation_taxlevel:
    - family
    - genus
    - species
calibration_fdr: best_f1 # could be max FDR, e.g. 0.05
calibration_function: max # mean/median/max threshold of all calibration samples
filtering_threshold: 2
no_evaluation: []
rank_to_sort_by: species

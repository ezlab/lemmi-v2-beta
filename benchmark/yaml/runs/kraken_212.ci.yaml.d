container: quay.io/ezlab/kraken_212_lemmi:v2.1_cv1
# ref size is best or all
ref_size: best
instance_name: ci_prok_gtdb
tmp: ci_tmp
distribute_cores_to_n_tasks: 1
pe: 1
other_params: []

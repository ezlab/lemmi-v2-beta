import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'cookieconsent'
import 'cookieconsent/build/cookieconsent.min.css'

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

import VueYandexMetrika from 'vue3-yandex-metrika'

var app = createApp(App)
/*
app.use(VueYandexMetrika, {
    id: xxxxxxxx,
    router: router,
    env: process.env.NODE_ENV
})*/

app.use(router).mount('#app')

window.cookieconsent.initialise({
    "palette": {"popup": {"background": "#444444"}, "button": {"background": "#bbbbb0"}},
    "theme": "classic"
})

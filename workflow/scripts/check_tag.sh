if [[ $(cat $LEMMI_ROOT/config/config.yaml | grep "strict_container_version" | head -n1 | cut -f2 -d ":") -ne " 0" ]]
then
    tag=$(git describe --tags)
    if [[ "$2" != "$tag" ]]
    then
        echo "Strict container version is enabled. Git tag ($tag) and all container versions must match. $1 version is $2." 1>&2
        echo "To ignore, set strict_container_version: 0 in the config file" 1>&2
    else
        false
    fi
else
    false
fi

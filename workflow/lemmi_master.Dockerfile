from ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update --fix-missing
RUN apt-get install git-all -y --fix-missing
WORKDIR /
RUN git clone https://github.com/wanyuac/readSimulator.git
WORKDIR /readSimulator/
RUN sed -i '/for paired-end reads/a \ \ \ \ read_pair_num = 1 if read_pair_num == 0 else read_pair_num' readSimulator.py
WORKDIR /
ADD resources/art_bin_MountRainier /art
ENV PATH="/art:${PATH}"
RUN git clone https://github.com/nick-youngblut/gtdb_to_taxdump
WORKDIR /gtdb_to_taxdump
RUN git checkout 0.1.5
WORKDIR /
RUN git clone https://github.com/lbobay/CoreSimul.git
RUN apt-get install python3-pip -y
RUN apt-get install libbz2-dev liblzma-dev -y
RUN pip3 install -U numpy joblib HTSeq pybedtools pysam scikit-learn==0.22 scipy six
RUN cp /usr/bin/python3 /usr/bin/python
RUN git clone https://github.com/bcgsc/NanoSim/
WORKDIR /NanoSim
RUN git checkout v3.0.2
RUN tar zxvf /NanoSim/pre-trained_models/metagenome_ERR3152366_Log.tar.gz
WORKDIR /
RUN apt update
RUN apt install cmake -y
RUN apt-get install samtools -y
RUN apt-get install wget -y
RUN apt-get install libgsl-dev -y 
RUN pip install biopython 
RUN apt install seq-gen -y
RUN git clone --recursive  --depth 1 https://github.com/grenaud/gargammel.git
WORKDIR /gargammel
RUN make src/fragSim 
RUN make src/deamSim 
RUN make src/adptSim
RUN make src/fasta2fastas
WORKDIR /
RUN apt-get install npm -y
ADD lemmi_frontend /lemmi_frontend
WORKDIR /lemmi_frontend
RUN npm install --force

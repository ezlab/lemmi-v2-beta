"""
This snakemake pipeline is the first to be called.
It will set up the LEMMI repository necessary for all subsequent steps
"""

import os

import numpy as np
import pandas as pd
import toolbox
from Bio import Entrez
from ete3 import NCBITaxa

Entrez.email = "A.N.Other@example.com"

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

# always set the tmp folder as working directory
os.chdir(tmp)

np.random.seed(config["lemmi_seed"])

# if running with docker, extra keywords are placed in the rules
# if calling --use-singularity, they will be set empty
container_runner, docker_container, clean = toolbox.define_docker_command(
    "LEMMI_master", "lemmi_master", root, tmp, config, 1
)


toolbox.validate_config(config)


localrules:
    target,


rule target:
    input:
        gtdb_taxdump=expand(
            "{root}repository/gtdb_taxdump.tar.gz",
            root=root,
        ),
        ncbi_dmp=expand(
            "{root}repository/taxdump.tar.gz",
            root=root,
        ),
        ete3=expand("{root}repository/.up_ete3", root=root),
        host_genome=expand(
            "{root}repository/host/{host_genome}",
            root=root,
            host_genome=config["host_genome"].split("/")[-1],
        ),
        gtdb_metadata=expand(
            "{root}repository/gtdb_metadata.gz",
            root=root,
        ),
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
        prok_list=expand("{root}repository/prok_list.tsv", root=root),
        prokncbi_list=expand("{root}repository/prokncbi_list.tsv", root=root),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),


rule create_prok_list:
    """
    Process the gtdb and genebank list of genomes to organise them for LEMMI
    and ensure each entry has all necessary metadata
    prok_list contains only accession overlapping GTDB and NCBI, so it can be compared.
    """
    input:
        gtdb_metadata=expand(
            "{root}repository/gtdb_metadata.gz",
            root=root,
        ),
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
        gtdb_names_dmp="names.dmp.tsv",
    output:
        prok_list=expand(
            "{root}repository/prok_list.tsv",
            root=root,
        ),
    message:
        "Creating the list of prokaryote genomes to be used..."
    threads: 1
    run:
        data = pd.read_csv(
            input.gtdb_metadata[0], compression="gzip", header=0, sep="\t"
        )
        ncbi_valid = pd.read_csv(
            input.genbank_assembly[0],
            header=0,
            sep="\t",
        )
        # change taxid in integer not float
        data = data.astype(
            {"ncbi_species_taxid": int, "ncbi_taxid": int}, errors="ignore"
        )
        # replace taxid with species taxid. Note this is a quick fix to avoid changing the field later in the code. species taxid should be used, but it wasn't.
        data["ncbi_taxid"] = data["ncbi_species_taxid"]
        # filter
        # those with no taxonomy
        data = data[data.gtdb_taxonomy.isna() == False]
        # those not matching the config, prok clade
        data = data[
            data.apply(
                lambda row: len(
                    set(row["gtdb_taxonomy"].split(";")) & set(config["prok_clade"])
                )
                > 0,
                axis=1,
            )
        ]
        # those with no equivalent in ncbi assembly
        data = data[
            data["ncbi_genbank_assembly_accession"].isin(
                ncbi_valid["# assembly_accession"]
            )
        ]
        # filter to keep those with at least two matches with the same gtdb taxo,  and write output
        data_f = data[
            data.gtdb_taxonomy.groupby(data.gtdb_taxonomy).transform(func=len) > 1
        ]
        # fetch the assembly level and refseq category to be used later
        ncbi_valid["ncbi_genbank_assembly_accession"] = ncbi_valid[
            "# assembly_accession"
        ]
        data_f = data_f.merge(ncbi_valid, on="ncbi_genbank_assembly_accession")
        # keep a given nb of genome per organism according to the defined limit
        data_f = data_f.groupby("gtdb_taxonomy").tail(
            int(config["max_genomes_per_organism"])
        )
        # add one field, the fake gtdb taxid
        gtdb_taxids = pd.read_csv(
            input.gtdb_names_dmp,
            header=0,
            sep="\t",
        )
        data_f = data_f.merge(gtdb_taxids, on="accession")
        data_f["__gtdb_accession"] = data_f["accession"]

        # with some version of pandas I think, the merge process is different, deal with it
        if 'genome_size' not in data_f.columns:
            data_f.loc[:, 'genome_size'] = data_f['genome_size_x']
        # write the output
        # header
        outp = open(output.prok_list[0], "w")
        outp.write("# LEMMI content selected from GTDB data\n")
        outp.close()
        data_f.to_csv(
            output.prok_list[0],
            sep="\t",
            header=True,
            index=False,
            columns=[
                data.columns[0],
                "genome_size",
                "gtdb_taxonomy",
                "ncbi_genbank_assembly_accession",
                "ncbi_species_taxid",
                "ncbi_strain_identifiers",
                "ncbi_taxid",
                "ncbi_taxonomy",
                "seq_rel_date",
                "__gtdb_accession",
                "assembly_level",
                "refseq_category",
                "__lineage",
                "__gtdb_taxid",
            ],
            mode="a",
        )


rule create_prokncbi_list:
    """
    prokncbi_list is solely based on NCBI and used with the prokncbi mode.
    It is useful when the last GTDB release is too old and recent genomes are needed
    """
    input:
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
    output:
        prokncbi_list=expand("{root}repository/prokncbi_list.tsv", root=root),
    message:
        "Creating the list of prokaryote (ncbi only) genomes to be used..."
    threads: 1
    run:
        toolbox.create_list("prokncbi", input, output, config)


rule create_euk_list:
    input:
        prokncbi_list=expand("{root}repository/prokncbi_list.tsv", root=root),
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
    output:
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
    message:
        "Creating the list of eukaryote genomes to be used..."
    threads: 1
    run:
        toolbox.create_list("euk", input, output, config)


rule create_vir_list:
    input:
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
    output:
        vir_list=expand(
            "{root}repository/vir_list.tsv",
            root=root,
        ),
    message:
        "Creating the list of viral genomes to be used..."
    threads: 1
    run:
        toolbox.create_list("vir", input, output, config)


rule download_dmp:
    output:
        ncbi_dmp=expand("{root}repository/taxdump.tar.gz", root=root),
    params:
        url=config["ncbi_dmp"],
        input_file=config["ncbi_dmp"].split("/")[-1],
    message:
        "Downloading the ncbi taxonomy dmp files..."
    threads: 1
    shell:
        """
        pwd
        mkdir -p tmp_dmp
        wget {params.url} --output-document tmp_dmp/{params.input_file}
        unzip -d tmp_dmp -o tmp_dmp/{params.input_file}
        rm tmp_dmp/{params.input_file}
        cd tmp_dmp
        tar zcvf {output.ncbi_dmp} *.*
        cd ..
        """


rule up_ete3:
    input:
        ncbi_dmp=expand(
            "{root}repository/taxdump.tar.gz",
            root=root,
        ),
    output:
        ete3=expand("{root}repository/.up_ete3", root=root),
    threads: 1
    message:
        "Updating the ete3 ncbi taxonomy database..."
    run:
        NCBITaxa().update_taxonomy_database(taxdump_file=str(input))
        open(str(output), "w").close()


rule download_host:
    output:
        host_genome=expand(
            "{root}repository/host/{host_genome}",
            root=root,
            host_genome=config["host_genome"].split("/")[-1],
        ),
    params:
        url=config["host_genome"],
    threads: 1
    message:
        "Downloading the host genome..."
    shell:
        """
        wget {params.url} --output-document {output.host_genome}
        rm {root}repository/genomes/host.fna.gz || true
        ln -s {output.host_genome} {root}repository/genomes/host.fna.gz
        """


rule annotate_genbank_lineage:
    """
    use the ete3 database to write the lineage in the ncbi assembly file
    also limit the entries kept to the specified date at the end of url
    """
    input:
        ete3=expand("{root}repository/.up_ete3", root=root),
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}.tmp",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
    output:
        genbank_assembly=expand(
            "{root}repository/{genbank_assembly}",
            root=root,
            genbank_assembly=config["genbank_assembly"].split("/")[-1].split("|")[0],
        ),
    message:
        "Filtering by date and annotating the genbank assemblies with its lineage names..."
    threads: 1
    run:
        data = pd.read_csv(input.genbank_assembly[0], header=1, sep="\t")
        # drop entries older than the date specified at the end of the file (for reproducibility purpose)
        data.drop(
            data[
                data["seq_rel_date"].str.replace("/", "")
                > config["genbank_assembly"].split("|")[-1]
            ].index,
            axis=0,
            inplace=True,
        )
        ncbi = NCBITaxa()


        def get_lineage(taxid):
            try:
                return ";".join(
                    [
                        list(ncbi.get_taxid_translator([v]).values())[0]
                        for v in ncbi.get_lineage(taxid)
                    ]
                )
            except ValueError:
                return 0


        data["__lineage"] = data.apply(lambda row: get_lineage(row["taxid"]), axis=1)
        # drop lines without lineage without ftp path
        data.drop(data[data["__lineage"] == 0].index, axis=0, inplace=True)
        data.drop(data[data["ftp_path"] == "na"].index, axis=0, inplace=True)
        # in some env the accession field lack a space
        if '# assembly_accession' not in data.columns:
            data.loc[:, '# assembly_accession'] = data['#assembly_accession'] 
        data.to_csv(output.genbank_assembly[0], sep="\t", header=True, index=False)


rule download_genbank:
    output:
        genbank_assembly=temp(
            expand(
                "{root}repository/{genbank_assembly}.tmp",
                root=root,
                genbank_assembly=config["genbank_assembly"]
                .split("/")[-1]
                .split("|")[0],
            )
        ),
    params:
        url=config["genbank_assembly"].split("|")[0],
    message:
        "Downloading the genbank assemblies..."
    threads: 1
    shell:
        """
        wget {params.url} --output-document {output.genbank_assembly}
        """


rule create_gtdb_ncbi_dmp:
    """
    LEMMI supports GTDB but through NCBI-like files 
    so it is invisible for the tools
    """
    input:
        gtdb_metadata=expand(
            "{root}repository/gtdb_metadata.gz",
            root=root,
        ),
    output:
        gtdb_taxdump=expand(
            "{root}repository/gtdb_taxdump.tar.gz",
            root=root,
        ),
        gtdb_names_dmp=temp("names.dmp.tsv"),
    container:
        "{}sif/{}.sif".format(root, config["lemmi_master"].split("/")[-1])
    message:
        "Converting GTDB taxonomy into ncbi-like dmp files..."
    threads: 1
    shell:
        """
        gunzip -c {input.gtdb_metadata} | tail -n+2 | cut -f1,17 > gtdb_taxonomy.tmp
        {container_runner} {docker_container} python3 /gtdb_to_taxdump/gtdb_to_taxdump.py gtdb_taxonomy.tmp
        {clean}
        rm gtdb_taxonomy.tmp
        tar zcvf {output.gtdb_taxdump} names.dmp nodes.dmp delnodes.dmp merged.dmp
        echo "__gtdb_taxid\taccession" > names.dmp.tsv
        cat names.dmp | cut -f1,3 >> names.dmp.tsv
        rm names.dmp nodes.dmp delnodes.dmp merged.dmp
        """


rule download_gtdb:
    output:
        gtdb_metadata=expand(
            "{root}repository/gtdb_metadata.gz",
            root=root,
        ),
    params:
        url=config["gtdb_metadata"],
        names=[n.split("/")[-1] for n in config["gtdb_metadata"]],
        names_unzipped=[
            "{}.tsv".format(n.split("/")[-1].split(".")[0])
            for n in config["gtdb_metadata"]
        ],
        untar=";".join(
            ["tar zxvf {}".format(n.split("/")[-1]) for n in config["gtdb_metadata"]]
        ),
    message:
        "Downloading the gtdb genome annotations..."
    threads: 1
    shell:
        """
        wget {params.url}
        # below, a few hack to fuse the two files
        {params.untar}
        head -n 1 {params.names_unzipped[0]} > gtdb_metadata # get the header line, once
        cat {params.names_unzipped} | grep -v accession >> gtdb_metadata # get rid of the header lines
        gzip gtdb_metadata
        mv gtdb_metadata.gz {output.gtdb_metadata}
        rm {params.names} {params.names_unzipped}
        """

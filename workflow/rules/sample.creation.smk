import toolbox
import os

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

os.chdir(tmp)
# deal with empty config category
if config["euk"] is None:
    config["euk"] = []
if config["prok"] is None:
    config["prok"] = []
if config["vir"] is None:
    config["vir"] = []
if config["host"] is None:
    config["host"] = []

container_runner, docker_container, clean = toolbox.define_docker_command(
    "LEMMI_master", "lemmi_master", root, tmp, config, 1
)

toolbox.validate_config(config)

real_target_files = [
    expand(
        "{root}instances/{instance_name}/{sample_name}/reads.r1.fastq.gz",
        root=root,
        instance_name=config["instance_name"],
        sample_name=config["sample_name"],
    ),
    expand(
        "{root}instances/{instance_name}/{sample_name}/reads.r2.fastq.gz",
        root=root,
        instance_name=config["instance_name"],
        sample_name=config["sample_name"],
    ),
]


localrules:
    target,


rule target:
    input:
        r1=expand(
            "{root}instances/{instance_name}/{sample_name}/reads.r1.fastq.gz",
            root=root,
            instance_name=config["instance_name"],
            sample_name=config["sample_name"],
        ),
        r2=expand(
            "{root}instances/{instance_name}/{sample_name}/reads.r2.fastq.gz",
            root=root,
            instance_name=config["instance_name"],
            sample_name=config["sample_name"],
        ),


rule gunzip_prok_genomes:
    input:
        prok=expand("{root}repository/genomes/{{prok}}.fna.gz", root=root, tmp=tmp),
    output:
        prok=temp(expand("{tmp}prok_{{prok}}.fasta", root=root, tmp=tmp)),
    message:
        "uncompressing a prok genome in tmp..."
    threads: 1
    shell:
        """
        gunzip -c {input.prok} > {output.prok}
        """


rule gunzip_euk_genomes:
    input:
        euk=expand("{root}repository/genomes/{{euk}}.fna.gz", root=root, tmp=tmp),
    output:
        euk=temp(expand("{tmp}euk_{{euk}}.fasta", root=root, tmp=tmp)),
    message:
        "uncompressing an euk genome in tmp..."
    threads: 1
    shell:
        """
        gunzip -c {input.euk} > {output.euk}
        """


rule gunzip_vir_genomes:
    input:
        vir=expand("{root}repository/genomes/{{vir}}.fna.gz", root=root, tmp=tmp),
    output:
        vir=temp(expand("{tmp}vir_{{vir}}.fasta", root=root, tmp=tmp)),
    message:
        "uncompressing a viral genome in tmp..."
    threads: 1
    shell:
        """
        gunzip -c {input.vir} > {output.vir}
        """


rule create_euk_reads:
    input:
        euk=toolbox.define_tmp_folder_rules_inputs(
            expand("{tmp}euk_{{euk}}.fasta", root=root, tmp=tmp),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
    output:
        r1_euk=temp(expand("{tmp}euk_{{euk}}_1.fastq.gz", root=root, tmp=tmp)),
        r2_euk=temp(expand("{tmp}euk_{{euk}}_2.fastq.gz", root=root, tmp=tmp)),
    container:
        "{}sif/{}.sif".format(root, config["lemmi_master"].split("/")[-1])
    params:
        f=lambda wildcards: [
            list(e.values())[0]
            for e in config["euk"]
            if list(e.keys())[0].split(':')[0] == wildcards.euk
        ],
        ancient=lambda wildcards: [int(':ancient' in list(e.keys())[0])
            for e in config["euk"]
            if list(e.keys())[0].split(':')[0] == wildcards.euk
        ],
        container_suffix=(
            lambda wildcards: wildcards.euk if container_runner != "" else ""
        ),
        container3_suffix=(
            lambda wildcards: "{}_3".format(wildcards.euk)
            if container_runner != ""
            else ""
        ),
        container4_suffix=(
            lambda wildcards: "{}_4".format(wildcards.euk)
            if container_runner != ""
            else ""
        ),
        container5_suffix=(
            lambda wildcards: "{}_5".format(wildcards.euk)
            if container_runner != ""
            else ""
        ),
        length=lambda wildcards: int(
            0.9
            * sum(
                [
                    len(l.replace("N", ""))
                    for l in open("{}euk_{}.fasta".format(tmp, wildcards.euk))
                    if not l.startswith(">")
                ]
            )
        ),
        readlen=config["read_length"],
        long_reads=config["long_reads"] if "long_reads" in config else 0,
        tech=config["tech"],
        max_base_quality=config["max_base_quality"],
        fragment_mean_size=config["fragment_mean_size"],
        fragment_sd=config["fragment_sd"],
    message:
        "Creating reads for an euk genome..."
    threads: 1
    shell:
        """
        if [ {params.ancient} == 1 ]
        then
        {container_runner}{params.container5_suffix} {docker_container} /gargammel/src/deamSim  --seed 1 -matfile /gargammel/src/matrices/double- -o {input.euk}_evolved.deamSim.fasta.gz {input.euk}_evolved
        gunzip -c {input.euk}_evolved.deamSim.fasta.gz > {input.euk}_evolved
        fi
        if [ {params.long_reads} == 1 ]
        then
        echo -e "Size\t{params.f}" > metagenome.{wildcards.euk}.tsv
        echo -e "{wildcards.euk}\t100" >> metagenome.{wildcards.euk}.tsv
        echo -e "{wildcards.euk}\t{input.euk}" > genome_list_{wildcards.euk}.tsv
        cat {input.euk} | grep ">" > dna_type_list.{wildcards.euk}.tsv
        sed -i'' -e 's/>/{wildcards.euk}\t/' dna_type_list.{wildcards.euk}.tsv
        sed -i'' -e 's/$/\tlinear/' dna_type_list.{wildcards.euk}.tsv
        touch genome_list_{wildcards.euk}.tsv metagenome.{wildcards.euk}.tsv dna_type_list.{wildcards.euk}.tsv
        {container_runner}{params.container_suffix} {docker_container} {root}../workflow/scripts/task_killer.sh python3 /NanoSim/src/simulator.py metagenome -gl genome_list_{wildcards.euk}.tsv -a metagenome.{wildcards.euk}.tsv -dl dna_type_list.{wildcards.euk}.tsv -c /NanoSim/metagenome_ERR3152366_Log/training -t 1 --fastq -b albacore -o lr_{wildcards.euk}
        cat lr_{wildcards.euk}*.fastq > {wildcards.euk}.tmp1
        echo "nothing" > {wildcards.euk}.tmp2
        gzip -c {wildcards.euk}.tmp1 > {output.r1_euk}
        gzip -c {wildcards.euk}.tmp2 > {output.r2_euk}
        else
        {container_runner}{params.container_suffix} {docker_container} python3 /readSimulator/readSimulator.py --input {input.euk} --simulator art --simulator_path /art/art_illumina --outdir . --readlen {params.readlen} --depth {params.f} --opts '-m {params.fragment_mean_size} -s {params.fragment_sd} -qU {params.max_base_quality} -nf 0 -rs 10 -ss {params.tech} -qs -1.5 -qs2 -2 -na'
        fi
        {clean}
        """


rule create_prok_reads:
    input:
        prok=toolbox.define_tmp_folder_rules_inputs(
            expand("{tmp}prok_{{prok}}.fasta", root=root, tmp=tmp),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
    output:
        r1_prok=temp(expand("{tmp}prok_{{prok}}_1.fastq.gz", root=root, tmp=tmp)),
        r2_prok=temp(expand("{tmp}prok_{{prok}}_2.fastq.gz", root=root, tmp=tmp)),
    container:
        "{}sif/{}.sif".format(root, config["lemmi_master"].split("/")[-1])
    params:
        f=lambda wildcards: [
            list(e.values())[0]
            for e in config["prok"]
            if list(e.keys())[0].split(':')[0] == wildcards.prok
        ],
        ancient=lambda wildcards: [int(':ancient' in list(e.keys())[0])
            for e in config["prok"]
            if list(e.keys())[0].split(':')[0] == wildcards.prok
        ],
        container_suffix=(
            lambda wildcards: wildcards.prok if container_runner != "" else ""
        ),
        container2_suffix=(
            lambda wildcards: "{}_2".format(wildcards.prok)
            if container_runner != ""
            else ""
        ),
        container3_suffix=(
            lambda wildcards: "{}_3".format(wildcards.prok)
            if container_runner != ""
            else ""
        ),
        container4_suffix=(
            lambda wildcards: "{}_4".format(wildcards.prok)
            if container_runner != ""
            else ""
        ),
        container5_suffix=(
            lambda wildcards: "{}_5".format(wildcards.prok)
            if container_runner != ""
            else ""
        ),
        readlen=config["read_length"],
        tech=config["tech"],
        max_base_quality=config["max_base_quality"],
        fragment_mean_size=config["fragment_mean_size"],
        fragment_sd=config["fragment_sd"],
        core_simul=config["core_simul"],
        long_reads=config["long_reads"] if "long_reads" in config else 0,
        length=lambda wildcards: int(
            0.9
            * sum(
                [
                    len(l.replace("N", ""))
                    for l in open("{}prok_{}.fasta".format(tmp, wildcards.prok))
                    if not l.startswith(">")
                ]
            )
        ),
    message:
        "Creating reads for a prok genome..."
    threads: 1
    shell:
        """
        if [ {params.core_simul} == 1 ]
        then
        cp ../../../resources/coresimul_control.txt coresimul_control.txt.{wildcards.prok}
        sed -i'' -e s/INSERT_OUTPUT_HERE/{wildcards.prok}_core_simul/ coresimul_control.txt.{wildcards.prok}
        sed -i'' -e s/INSERT_SEQUENCE_HERE/prok_{wildcards.prok}.fasta/ coresimul_control.txt.{wildcards.prok}
        sed -i'' -e s/[WSMKRYBDHVN]//g prok_{wildcards.prok}.fasta
        sed -i'' -e "s/INSERT_LENGTH_HERE/{params.length}/" coresimul_control.txt.{wildcards.prok}
        {container_runner}{params.container_suffix} {docker_container} python3 /CoreSimul/coresimul_master.py coresimul_control.txt.{wildcards.prok}
        head -n 2 {wildcards.prok}_core_simul/genomes.fa > {input.prok}_evolved
        sed -i'' -e s/-//g {input.prok}_evolved
        else
        mv {input.prok} {input.prok}_evolved
        fi
        if [ {params.ancient} == 1 ]
        then
        {container_runner}{params.container5_suffix} {docker_container} /gargammel/src/deamSim  --seed 1 -matfile /gargammel/src/matrices/double- -o {input.prok}_evolved.deamSim.fasta.gz {input.prok}_evolved
        gunzip -c {input.prok}_evolved.deamSim.fasta.gz > {input.prok}_evolved
        fi
        if [ {params.long_reads} == 1 ]
        then
        echo -e "Size\t{params.f}" > metagenome.{wildcards.prok}.tsv
        echo -e "{wildcards.prok}\t100" >> metagenome.{wildcards.prok}.tsv
        echo -e "{wildcards.prok}\t{input.prok}_evolved" > genome_list_{wildcards.prok}.tsv
        cat {input.prok}_evolved | grep ">" > dna_type_list.{wildcards.prok}.tsv
        sed -i'' -e 's/>/{wildcards.prok}\t/' dna_type_list.{wildcards.prok}.tsv
        sed -i'' -e 's/$/\tlinear/' dna_type_list.{wildcards.prok}.tsv
        touch genome_list_{wildcards.prok}.tsv metagenome.{wildcards.prok}.tsv dna_type_list.{wildcards.prok}.tsv
        {container_runner}{params.container2_suffix} {docker_container} {root}../workflow/scripts/task_killer.sh python3 /NanoSim/src/simulator.py metagenome -gl genome_list_{wildcards.prok}.tsv -a metagenome.{wildcards.prok}.tsv -dl dna_type_list.{wildcards.prok}.tsv -c /NanoSim/metagenome_ERR3152366_Log/training -t 1 --fastq -b albacore -o lr_{wildcards.prok}
        cat lr_{wildcards.prok}*.fastq > {wildcards.prok}.tmp1
        echo "nothing" > {wildcards.prok}.tmp2
        gzip -c {wildcards.prok}.tmp1 > {output.r1_prok}
        gzip -c {wildcards.prok}.tmp2 > {output.r2_prok}
        else
        {container_runner}{params.container2_suffix} {docker_container} python3 /readSimulator/readSimulator.py --input {input.prok}_evolved --simulator art --simulator_path /art/art_illumina --outdir . --readlen {params.readlen} --depth {params.f} --opts '-m {params.fragment_mean_size} -s {params.fragment_sd} -qU {params.max_base_quality} -nf 0 -rs 10 -ss {params.tech} -qs -1.5 -qs2 -2 -na'
        fi
        {clean}
        """


rule create_host_reads:
    input:
        host=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{root}repository/host/{host}",
                root=root,
                host=config["host_genome"].split("/")[-1],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
    output:
        r1_host=temp(
            expand(
                "{tmp}{host_acc}_1.fastq.gz",
                root=root,
                tmp=tmp,
                host_acc=".".join(
                    config["host_genome"].split("/")[-1].split(".")[0:-2]
                ),
            )
        ),
        r2_host=temp(
            expand(
                "{tmp}{host_acc}_2.fastq.gz",
                root=root,
                tmp=tmp,
                host_acc=".".join(
                    config["host_genome"].split("/")[-1].split(".")[0:-2]
                ),
            )
        ),
    container:
        "{}sif/{}.sif".format(root, config["lemmi_master"].split("/")[-1])
    params:
        f=config["host"],
        unzipped_name=".".join(config["host_genome"].split("/")[-1].split(".")[0:-1]),
        container_suffix=(
            ".".join(config["host_genome"].split("/")[-1].split(".")[0:-1])
            if container_runner != ""
            else ""
        ),
        readlen=config["read_length"],
        long_reads=config["long_reads"] if "long_reads" in config else 0,
        tech=config["tech"],
        max_base_quality=config["max_base_quality"],
        fragment_mean_size=config["fragment_mean_size"],
        fragment_sd=config["fragment_sd"],
        host_acc=".".join(config["host_genome"].split("/")[-1].split(".")[0:-2]),
    message:
        "Creating reads for the host..."
    threads: 1
    shell:
        """
        gunzip -c {input.host} > {params.unzipped_name}
        if [ {params.long_reads} == 1 ]
        then
        echo -e "Size\t{params.f}" > metagenome.{params.host_acc}.tsv
        echo -e "{params.host_acc}\t100" >> metagenome.{params.host_acc}.tsv
        echo -e "{params.host_acc}\t{params.unzipped_name}" > genome_list_{params.host_acc}.tsv
        cat {params.unzipped_name} | grep ">" > dna_type_list.{params.host_acc}.tsv
        sed -i'' -e 's/>/{params.host_acc}\t/' dna_type_list.{params.host_acc}.tsv
        sed -i'' -e 's/$/\tlinear/' dna_type_list.{params.host_acc}.tsv
        touch genome_list_{params.host_acc}.tsv metagenome.{params.host_acc}.tsv dna_type_list.{params.host_acc}.tsv
        {container_runner}{params.container_suffix} {docker_container} {root}../workflow/scripts/task_killer.sh python3 /NanoSim/src/simulator.py metagenome -gl genome_list_{params.host_acc}.tsv -a metagenome.{params.host_acc}.tsv -dl dna_type_list.{params.host_acc}.tsv -c /NanoSim/metagenome_ERR3152366_Log/training -t 1 --fastq -b albacore -o lr_{params.host_acc}
        cat lr_{params.host_acc}*.fastq > {params.host_acc}.tmp1
        echo "nothing" > {params.host_acc}.tmp2
        gzip -c {params.host_acc}.tmp1 > {output.r1_host}
        gzip -c {params.host_acc}.tmp2 > {output.r2_host}
        else
        {container_runner}{params.container_suffix} {docker_container} python3 /readSimulator/readSimulator.py --input {params.unzipped_name} --simulator art --simulator_path /art/art_illumina --outdir . --readlen {params.readlen} --depth {params.f} --opts '-m {params.fragment_mean_size} -s {params.fragment_sd} -qU {params.max_base_quality} -nf 0 -rs 10 -ss {params.tech} -qs -1.5 -qs2 -2 -na'
        rm {params.unzipped_name}
        fi
        {clean}
        """


rule create_vir_reads:
    input:
        vir=toolbox.define_tmp_folder_rules_inputs(
            expand("{tmp}vir_{{vir}}.fasta", root=root, tmp=tmp),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
    output:
        r1_vir=temp(expand("{tmp}vir_{{vir}}_1.fastq.gz", root=root, tmp=tmp)),
        r2_vir=temp(expand("{tmp}vir_{{vir}}_2.fastq.gz", root=root, tmp=tmp)),
    container:
        "{}sif/{}.sif".format(root, config["lemmi_master"].split("/")[-1])
    params:
        f=lambda wildcards: [
            list(e.values())[0]
            for e in config["vir"]
            if list(e.keys())[0].split(':')[0] == wildcards.vir
        ],
        ancient=lambda wildcards: [int(':ancient' in list(e.keys())[0])
            for e in config["vir"]
            if list(e.keys())[0].split(':')[0] == wildcards.vir
        ],
        container_suffix=(
            lambda wildcards: wildcards.vir if container_runner != "" else ""
        ),
        container2_suffix=(
            lambda wildcards: "{}_2".format(wildcards.vir)
            if container_runner != ""
            else ""
        ),
        container3_suffix=(
            lambda wildcards: "{}_3".format(wildcards.vir)
            if container_runner != ""
            else ""
        ),
        container4_suffix=(
            lambda wildcards: "{}_4".format(wildcards.vir)
            if container_runner != ""
            else ""
        ),
        container5_suffix=(
            lambda wildcards: "{}_5".format(wildcards.vir)
            if container_runner != ""
            else ""
        ),
        readlen=config["read_length"],
        long_reads=config["long_reads"] if "long_reads" in config else 0,
        tech=config["tech"],
        max_base_quality=config["max_base_quality"],
        fragment_mean_size=config["fragment_mean_size"],
        fragment_sd=config["fragment_sd"],
        core_simul=config["core_simul"],
        length=lambda wildcards: int(
            0.9
            * sum(
                [
                    len(l.replace("N", ""))
                    for l in open("{}vir_{}.fasta".format(tmp, wildcards.vir))
                    if not l.startswith(">")
                ]
            )
        ),
    message:
        "Creating reads for a viral genome..."
    threads: 1
    shell:
        """
        if [ {params.core_simul} == 1 ]
        then
        cp ../../../resources/coresimul_control.txt coresimul_control.txt.{wildcards.vir}
        sed -i'' -e s/INSERT_OUTPUT_HERE/{wildcards.vir}_core_simul/ coresimul_control.txt.{wildcards.vir}
        sed -i'' -e s/INSERT_SEQUENCE_HERE/vir_{wildcards.vir}.fasta/ coresimul_control.txt.{wildcards.vir}
        sed -i'' -e s/[WSMKRYBDHVN]//g vir_{wildcards.vir}.fasta
        sed -i'' -e "s/INSERT_LENGTH_HERE/{params.length}/" coresimul_control.txt.{wildcards.vir}
        {container_runner}{params.container_suffix} {docker_container} python3 /CoreSimul/coresimul_master.py coresimul_control.txt.{wildcards.vir}
        head -n 2 {wildcards.vir}_core_simul/genomes.fa > {input.vir}_evolved
        sed -i'' -e s/-//g {input.vir}_evolved
        else
        mv {input.vir} {input.vir}_evolved
        fi
        if [ {params.ancient} == 1 ]
        then
        {container_runner}{params.container5_suffix} {docker_container} /gargammel/src/deamSim  --seed 1 -matfile /gargammel/src/matrices/double- -o {input.vir}_evolved.deamSim.fasta.gz {input.vir}_evolved
        gunzip -c {input.vir}_evolved.deamSim.fasta.gz > {input.vir}_evolved
        fi
        if [ {params.long_reads} == 1 ]
        then
        echo -e "Size\t{params.f}" > metagenome.{wildcards.vir}.tsv
        echo -e "{wildcards.vir}\t100" >> metagenome.{wildcards.vir}.tsv
        echo -e "{wildcards.vir}\t{input.vir}_evolved" > genome_list_{wildcards.vir}.tsv
        cat {input.vir}_evolved | grep ">" > dna_type_list.{wildcards.vir}.tsv
        sed -i'' -e 's/>/{wildcards.vir}\t/' dna_type_list.{wildcards.vir}.tsv
        sed -i'' -e 's/$/\tlinear/' dna_type_list.{wildcards.vir}.tsv
        touch genome_list_{wildcards.vir}.tsv metagenome.{wildcards.vir}.tsv dna_type_list.{wildcards.vir}.tsv
        {container_runner}{params.container2_suffix} {docker_container} {root}../workflow/scripts/task_killer.sh python3 /NanoSim/src/simulator.py metagenome -gl genome_list_{wildcards.vir}.tsv -a metagenome.{wildcards.vir}.tsv -dl dna_type_list.{wildcards.vir}.tsv -c /NanoSim/metagenome_ERR3152366_Log/training -t 1 --fastq -b albacore -o lr_{wildcards.vir}
        cat lr_{wildcards.vir}*.fastq > {wildcards.vir}.tmp1
        echo "nothing" > {wildcards.vir}.tmp2
        gzip -c {wildcards.vir}.tmp1 > {output.r1_vir}
        gzip -c {wildcards.vir}.tmp2 > {output.r2_vir}
        else
        {container_runner}{params.container2_suffix} {docker_container} python3 /readSimulator/readSimulator.py --input {input.vir}_evolved --simulator art --simulator_path /art/art_illumina --outdir . --readlen {params.readlen} --depth {params.f} --opts '-m {params.fragment_mean_size} -s {params.fragment_sd} -qU {params.max_base_quality} -nf 0 -rs 10 -ss {params.tech} -qs -1.5 -qs2 -2 -na'
        fi
        {clean}
        """


rule anonymerge_reads:
    """
    for now just merge, mix and anonymze later if necessary
    """
    input:
        r1_euk=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}euk_{euk}_1.fastq.gz",
                root=root,
                tmp=tmp,
                euk=[list(e.keys())[0].split(':')[0] for e in config["euk"]],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        r2_euk=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}euk_{euk}_2.fastq.gz",
                root=root,
                tmp=tmp,
                euk=[list(e.keys())[0].split(':')[0] for e in config["euk"]],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        r1_prok=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}prok_{prok}_1.fastq.gz",
                root=root,
                tmp=tmp,
                prok=[list(e.keys())[0].split(':')[0] for e in config["prok"]],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        r2_prok=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}prok_{prok}_2.fastq.gz",
                root=root,
                tmp=tmp,
                prok=[list(e.keys())[0].split(':')[0] for e in config["prok"]],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        r1_vir=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}vir_{vir}_1.fastq.gz",
                root=root,
                tmp=tmp,
                vir=[list(e.keys())[0].split(':')[0] for e in config["vir"]],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        r2_vir=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}vir_{vir}_2.fastq.gz",
                root=root,
                tmp=tmp,
                vir=[list(e.keys())[0].split(':')[0] for e in config["vir"]],
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        r1_host=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}{host_acc}_1.fastq.gz",
                root=root,
                tmp=tmp,
                host_acc=".".join(
                    config["host_genome"].split("/")[-1].split(".")[0:-2]
                ),
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
        r2_host=toolbox.define_tmp_folder_rules_inputs(
            expand(
                "{tmp}{host_acc}_2.fastq.gz",
                root=root,
                tmp=tmp,
                host_acc=".".join(
                    config["host_genome"].split("/")[-1].split(".")[0:-2]
                ),
            ),
            "{}../workflow/rules/mock_input_file.txt".format(root),
            real_target_files,
        ),
    output:
        r1=expand(
            "{root}instances/{instance_name}/{sample_name}/reads.r1.fastq.gz",
            root=root,
            instance_name=config["instance_name"],
            sample_name=config["sample_name"],
        ),
        r2=expand(
            "{root}instances/{instance_name}/{sample_name}/reads.r2.fastq.gz",
            root=root,
            instance_name=config["instance_name"],
            sample_name=config["sample_name"],
        ),
    message:
        "Merging reads of all organims..."
    threads: 1
    shell:
        """
        gunzip -c {input.r1_prok} {input.r1_host} {input.r1_euk} {input.r1_vir} > {output.r1}.tmp
        gunzip -c {input.r2_prok} {input.r2_host} {input.r2_euk} {input.r2_vir} > {output.r2}.tmp
        gzip -c {output.r1}.tmp > {output.r1}
        gzip -c {output.r2}.tmp > {output.r2}
        rm {output.r1}.tmp {output.r2}.tmp
        """

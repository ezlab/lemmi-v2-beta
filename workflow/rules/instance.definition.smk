import random

import numpy as np
import pandas as pd

# from ete3 import NCBITaxa
import toolbox
from Bio import Entrez

Entrez.email = "A.N.Other@example.com"

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

os.chdir(tmp)

# double the nb of reads as art consider coverage for r1 and r2 separately.
if not ("long_reads" in config and config["long_reads"] == 1):
    config["total_reads_nb"] = 2 * config["total_reads_nb"]

# check if the ncbi prok list should be used
prok_suffix = ""
use_prokncbi = False
if config["use_prokncbi"] == 1:
    prok_suffix = "ncbi"
    use_prokncbi = True

toolbox.validate_config(config)


localrules:
    target,


rule target:
    input:
        evaluation=expand(
            "{root}yaml/instances/samples/{instance_name}-e{e}.yaml",
            root=root,
            instance_name=config["instance_name"],
            e=config["evaluation"],
        ),
        calibration=expand(
            "{root}yaml/instances/samples/{instance_name}-c{c}.yaml",
            root=root,
            instance_name=config["instance_name"],
            c=config["calibration"],
        ),
        negative=expand(
            "{root}yaml/instances/samples/{instance_name}-n{n}.yaml",
            root=root,
            instance_name=config["instance_name"],
            n=config["negative"],
        ),
        ref_prok_all_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.prok.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_prok_best_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.prok.best_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_euk_all_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.euk.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_euk_best_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.euk.best_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_vir_all_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.vir.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_vir_best_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.vir.best_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),


rule do_sampling:
    input:
        host_genome=expand(
            "{root}repository/host/{host_genome}",
            root=root,
            host_genome=config["host_genome"].split("/")[-1],
        ),
        prok_list=expand(
            "{root}repository/prok{prok_suffix}_list.tsv",
            root=root,
            prok_suffix=prok_suffix,
        ),
        euk_list=expand("{root}repository/euk_list.tsv", root=root),
        vir_list=expand("{root}repository/vir_list.tsv", root=root),
    output:
        evaluation=expand(
            "{root}yaml/instances/samples/{instance_name}-e{e}.yaml",
            root=root,
            instance_name=config["instance_name"],
            e=config["evaluation"],
        ),
        calibration=expand(
            "{root}yaml/instances/samples/{instance_name}-c{c}.yaml",
            root=root,
            instance_name=config["instance_name"],
            c=config["calibration"],
        ),
        negative=expand(
            "{root}yaml/instances/samples/{instance_name}-n{n}.yaml",
            root=root,
            instance_name=config["instance_name"],
            n=config["negative"],
        ),
        ref_prok_all_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.prok.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_prok_best_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.prok.best_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_euk_all_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.euk.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_euk_best_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.euk.best_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_vir_all_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.vir.all_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        ref_vir_best_ref=expand(
            "{root}instances/{instance_name}/{instance_name}.vir.best_ref.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
        unknown_lineages=expand(
            "{root}instances/{instance_name}/{instance_name}.unknown_lineages.tsv",
            root=root,
            instance_name=config["instance_name"],
        ),
    message:
        "Define the composition of each sample in the instance and the corresponding references..."
    threads: 1
    run:
        np.random.seed(config["instance_seed"])
        random.seed(config["instance_seed"])
        host_size = toolbox.get_genome_size(input.host_genome[0])

        prok_list = pd.read_csv(
            input.prok_list[0], dtype={"ncbi_taxid": np.int32}, sep="\t", header=1
        )
        euk_list = pd.read_csv(
            input.euk_list[0], dtype={"ncbi_taxid": np.int32}, sep="\t", header=1
        )
        vir_list = pd.read_csv(
            input.vir_list[0], dtype={"ncbi_taxid": np.int32}, sep="\t", header=1
        )
        # filter out genomes that appears in the 404 list (genomes that could not be downloaded)
        in_404 = set(
            [line.strip() for line in open("{}repository/genomes/404.txt".format(root))]
        )
        prok_list.drop(
            prok_list[prok_list["ncbi_genbank_assembly_accession"].isin(in_404)].index,
            axis=0,
            inplace=True,
        )
        euk_list.drop(
            euk_list[euk_list["ncbi_genbank_assembly_accession"].isin(in_404)].index,
            axis=0,
            inplace=True,
        )
        vir_list.drop(
            vir_list[vir_list["ncbi_genbank_assembly_accession"].isin(in_404)].index,
            axis=0,
            inplace=True,
        )

        # remove one randomly selected entry for each lineage in each of the list
        # so it cannot be selected in any sample and will remain for the reference for sure
        # to achieve that, shuffle and then remove the last duplicate,
        prok_list = prok_list.sample(frac=1)
        euk_list = euk_list.sample(frac=1)
        vir_list = vir_list.sample(frac=1)
        # obtain one value for each
        if use_prokncbi:
            prok_list_unique = prok_list.drop_duplicates(subset=["__lineage"])
        else:
            prok_list_unique = prok_list.drop_duplicates(subset=["gtdb_taxonomy"])
        euk_list_unique = euk_list.drop_duplicates(subset=["__lineage"])
        vir_list_unique = vir_list.drop_duplicates(subset=["__lineage"])
        # drop them
        prok_list_subset = prok_list.drop(prok_list_unique.index, inplace=False, axis=0)
        euk_list_subset = euk_list.drop(euk_list_unique.index, inplace=False, axis=0)
        vir_list_subset = vir_list.drop(vir_list_unique.index, inplace=False, axis=0)
        # if there is a constrain on the date, use a different strategy, split in before and after the date
        if "sampling_date_limit" in config and len(config["sampling_date_limit"]) == 10:
            prok_list_before = prok_list[
                prok_list["seq_rel_date"] <= config["sampling_date_limit"]
            ]
            vir_list_before = vir_list[
                vir_list["seq_rel_date"] <= config["sampling_date_limit"]
            ]
            euk_list_before = euk_list[
                euk_list["seq_rel_date"] <= config["sampling_date_limit"]
            ]
            prok_list_after = prok_list[
                prok_list["seq_rel_date"] > config["sampling_date_limit"]
            ]
            vir_list_after = vir_list[
                vir_list["seq_rel_date"] > config["sampling_date_limit"]
            ]
            euk_list_after = euk_list[
                euk_list["seq_rel_date"] > config["sampling_date_limit"]
            ]
            # drop from after lineages that do not exist in before
            lineages_to_keep = prok_list_before["__lineage"].unique()
            prok_list_after = prok_list_after[
                prok_list_after["__lineage"].isin(lineages_to_keep)
            ]
            lineages_to_keep = vir_list_before["__lineage"].unique()
            vir_list_after = vir_list_after[
                vir_list_after["__lineage"].isin(lineages_to_keep)
            ]
            lineages_to_keep = euk_list_before["__lineage"].unique()
            euk_list_after = euk_list_after[
                euk_list_after["__lineage"].isin(lineages_to_keep)
            ]
            # redefine the list, subset is for sampling, normal list is for ref
            prok_list_subset = prok_list_after
            vir_list_subset = vir_list_after
            euk_list_subset = euk_list_after
            prok_list = prok_list_before
            vir_list = vir_list_before
            euk_list = euk_list_before
        elif "sampling_date_limit" in config and config["sampling_date_limit"] != "none":
            exit("Please provide a valid value for sampling_date_limit. Either YYYY/MM/DD or none. You can also remove the value.")


        # list all accession matching an ancient lineage
        taxa = []
        try:
            taxa += config["prok_taxa"]
        except TypeError:
            pass

        try:
            taxa += config["euk_taxa"]
        except TypeError:
            pass

        try:
            taxa += config["vir_taxa"]
        except TypeError:
            pass

        ancient_lineages = [t.split(':ancient')[0] for t in taxa if ":ancient" in t]
        ancient_lineage_accession = []
        if len(ancient_lineages) > 0:
            ancient_lineage_accession = euk_list[euk_list["__lineage"].str.contains('|'.join(ancient_lineages))]['ncbi_genbank_assembly_accession'].tolist()
            ancient_lineage_accession += prok_list[prok_list["__lineage"].str.contains('|'.join(ancient_lineages))]['ncbi_genbank_assembly_accession'].tolist()
            ancient_lineage_accession += vir_list[vir_list["__lineage"].str.contains('|'.join(ancient_lineages))]['ncbi_genbank_assembly_accession'].tolist()
        selected_ids = {}
        for f in output.evaluation:
            selected_ids = toolbox.do_selection(
                f,
                prok_list_subset,
                euk_list_subset,
                vir_list_subset,
                selected_ids,
                host_size,
                config,
                ancient_lineage_accession
            )
        for f in output.calibration:
            selected_ids = toolbox.do_selection(
                f,
                prok_list_subset,
                euk_list_subset,
                vir_list_subset,
                selected_ids,
                host_size,
                config,
                ancient_lineage_accession
            )

        for f in output.negative:
            selected_ids = toolbox.do_selection(
                f,
                prok_list_subset,
                euk_list_subset,
                vir_list_subset,
                selected_ids,
                host_size,
                config,
                ancient_lineage_accession,
                negative=True,
            )

            # now define what will become the unknown, as defined in the config
        unknown_taxa = set([])
        if type(config["prok_taxa"]) == list:
            unknown_taxa.update(
                [
                    e.split(":")[0]
                    for e in config["prok_taxa"]
                    if e.endswith(":unknown")
                ]
            )
        if type(config["euk_taxa"]) == list:
            unknown_taxa.update(
                [
                    e.split(":")[0]
                    for e in config["euk_taxa"]
                    if e.endswith(":unknown")
                ]
            )
        if type(config["vir_taxa"]) == list:
            unknown_taxa.update(
                [
                    e.split(":")[0]
                    for e in config["vir_taxa"]
                    if e.endswith(":unknown")
                ]
            )
            # below, y for y is used to get rid of the 'd__' prefix, as it does not appear in the list of unknown taxa
            # x splits the lineage into individual rank
            # so if any of the rank is in the list of unknown taxa, the lineage is added to the list of unknown lineages
        unknown_lineages = [
            s
            for s in list(selected_ids.keys())
            if any(
                x in [y.split("__")[-1] for y in str(s).split(";")]
                for x in unknown_taxa
            )
        ]
        # keep track of them in a separate file
        with open(output.unknown_lineages[0], "w") as unknown_lineages_file:
            for lineage in unknown_lineages:
                for accession in selected_ids[lineage]:
                    unknown_lineages_file.write("{}\n".format(accession))
                    # update the selected_ids to exclude from ref with all accession matching the excluded lineages
        for lineage in unknown_lineages:
            # lineage is gtdb_taxonomy or ncbi taxid, not lineage
            if use_prokncbi:
                selected_ids[lineage] += list(
                    prok_list[
                        prok_list["taxid"] == lineage
                    ].ncbi_genbank_assembly_accession.values
                )
            else:
                selected_ids[lineage] += list(
                    prok_list[
                        prok_list["gtdb_taxonomy"] == lineage
                    ].ncbi_genbank_assembly_accession.values
                )
            selected_ids[lineage] += list(
                euk_list[
                    euk_list["taxid"] == lineage
                ].ncbi_genbank_assembly_accession.values
            )
            selected_ids[lineage] += list(
                vir_list[
                    vir_list["taxid"] == lineage
                ].ncbi_genbank_assembly_accession.values
            )

            # now define the reference. first the big one "all", anything not in reads

        if use_prokncbi:
            prok_list_ref = toolbox.create_full_ref(
                prok_list,
                selected_ids,
                output.ref_prok_all_ref[0],
                ["ncbi_genbank_assembly_accession", "taxid", "__lineage", "__size"],
                unknown_lineages,
            )
        else:
            prok_list_ref = toolbox.create_full_ref(
                prok_list,
                selected_ids,
                output.ref_prok_all_ref[0],
                [
                    "ncbi_genbank_assembly_accession",
                    "ncbi_taxid",
                    "ncbi_taxonomy",
                    "__lineage",
                    "gtdb_taxonomy",
                    "__gtdb_taxid",
                    "__gtdb_accession",
                    "genome_size",
                ],
                unknown_lineages,
            )
        euk_list_ref = toolbox.create_full_ref(
            euk_list,
            selected_ids,
            output.ref_euk_all_ref[0],
            ["ncbi_genbank_assembly_accession", "taxid", "__lineage", "__size"],
            unknown_lineages,
        )
        vir_list_ref = toolbox.create_full_ref(
            vir_list,
            selected_ids,
            output.ref_vir_all_ref[0],
            ["ncbi_genbank_assembly_accession", "taxid", "__lineage", "__size"],
            unknown_lineages,
        )

        # then a best, one representative by taxid
        if use_prokncbi:
            toolbox.create_best_ref(
                prok_list_ref,
                output.ref_prok_best_ref[0],
                ["taxid", "refseq_category", "assembly_level", "__size"],
                ["ncbi_genbank_assembly_accession", "taxid", "__lineage", "__size"],
            )
        else:
            toolbox.create_best_ref(
                prok_list_ref,
                output.ref_prok_best_ref[0],
                ["ncbi_taxid", "refseq_category", "assembly_level", "genome_size"] if config["targets_taxonomy"] == 'ncbi' else ["gtdb_taxonomy", "refseq_category", "assembly_level", "genome_size"],
                [
                    "ncbi_genbank_assembly_accession",
                    "ncbi_taxid",
                    "ncbi_taxonomy",
                    "gtdb_taxonomy",
                    "__gtdb_taxid",
                    "__lineage",
                    "__gtdb_accession",
                    "genome_size",
                ],
            )
        toolbox.create_best_ref(
            vir_list_ref,
            output.ref_vir_best_ref[0],
            ["taxid", "refseq_category", "assembly_level", "__size"],
            ["ncbi_genbank_assembly_accession", "taxid", "__lineage", "__size"],
        )
        toolbox.create_best_ref(
            euk_list_ref,
            output.ref_euk_best_ref[0],
            ["taxid", "refseq_category", "assembly_level", "__size"],
            ["ncbi_genbank_assembly_accession", "taxid", "__lineage", "__size"],
        )

#!/bin/sh
set -eu
export LEMMI_ROOT=$(pwd)/
mv $LEMMI_ROOT/config/config.yaml.ci $LEMMI_ROOT/config/config.yaml
mv $LEMMI_ROOT/benchmark/yaml/instances/ci_prok_gtdb.yaml.d $LEMMI_ROOT/benchmark/yaml/instances/ci_prok_gtdb.yaml
rm $LEMMI_ROOT/benchmark/yaml/instances/demo_prok_gtdb.yaml
mv $LEMMI_ROOT/benchmark/yaml/runs/kraken_212.ci.yaml.d $LEMMI_ROOT/benchmark/yaml/runs/kraken_212.ci.yaml
rm $LEMMI_ROOT/benchmark/yaml/runs/kraken_212.demo.yaml
$LEMMI_ROOT/workflow/scripts/lemmi_full --cores 1
# here we just test for the presence of the expected files. If any missing, ci fail
cd ${LEMMI_ROOT}/tests/expected/
for i in $(ls -d instances/ci_prok_gtdb/*);do ls ${LEMMI_ROOT}/benchmark/$i;done
for i in $(ls -d instances/ci_prok_gtdb/ci_prok_gtdb-?001/*);do ls ${LEMMI_ROOT}/benchmark/$i;done
for i in $(ls -d analysis_outputs/*);do ls ${LEMMI_ROOT}/benchmark/$i;done
for i in $(ls -d evaluations/*);do ls ${LEMMI_ROOT}/benchmark/$i;done
for i in $(ls -d final_results/*);do ls ${LEMMI_ROOT}/benchmark/$i;done


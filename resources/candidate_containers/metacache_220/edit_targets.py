#!/usr/bin/env python3 
import sys, gzip
open('metacache_targets_input.fasta', 'w').close()
acc2taxid=open('acc2taxid_targets.txt', 'w')
acc2taxid.write('accession\taccession.version\ttaxid\tgi\n')
acc2taxid.close()
outp=open('metacache_targets_input.fasta', 'a')
outp2=open('acc2taxid_targets.txt', 'a')
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split('\t')[0]
    outp2.write('{}\t1\t{}\t{}\n'.format(acc, taxid, acc))
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>{}|{}\n'.format(acc, acc)
        outp.write(line)
outp.close()
outp2.close()

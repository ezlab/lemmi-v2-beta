#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
gunzip -c ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz > reads.r1.fastq
gunzip -c ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz > reads.r2.fastq
/metacache/metacache query ../ref_host reads.r1.fastq reads.r2.fastq -pairfiles -out host.out -lowest species -mapped-only -taxids -separate-cols -threads $cpus
filter_out.py reads.r1.fastq host.out reads.r1.nohost.fastq
filter_out.py reads.r2.fastq host.out reads.r2.nohost.fastq
/metacache/metacache query ../ref_contaminants_$3 reads.r1.nohost.fastq reads.r2.nohost.fastq -pairfiles -out conta.out -lowest species -mapped-only -taxids -separate-cols -threads $cpus
filter_out.py reads.r1.nohost.fastq conta.out reads.r1.noconta.fastq
filter_out.py reads.r2.nohost.fastq conta.out reads.r2.noconta.fastq
/metacache/metacache query ../ref_targets_$3 reads.r1.noconta.fastq reads.r2.noconta.fastq -pairfiles -out targets.out -lowest species -mapped-only -taxids -separate-cols -threads $cpus -abundances targets.abundance -abundance-per species
cat targets.abundance | grep -v "#\|unclassified" | cut -f7 > reads
cat targets.abundance | grep -v "#\|unclassified" | cut -f5 > taxids
cat targets.abundance | grep -v "#\|unclassified" | cut -f3 > organisms
echo -e "taxid\treads" > reads_abundance
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
paste taxids reads >> reads_abundance
abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/references.tsv ../ref_targets_$instance/taxonomy/names.dmp abundance
paste organisms taxids reads abundance >> $output_file


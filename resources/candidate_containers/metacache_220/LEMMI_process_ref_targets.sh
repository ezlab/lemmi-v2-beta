#!/usr/bin/env bash
set -o xtrace
set -e
mkdir -p $2/taxonomy
mkdir -p $2/library
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2/taxonomy
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2/taxonomy
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
edit_targets.py $1 $3
cp $1  $2/references.tsv
/metacache/metacache build $2 metacache_targets_input.fasta -taxonomy $2/taxonomy -taxpostmap acc2taxid_targets.txt -kmerlen 32

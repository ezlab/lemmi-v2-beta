#!/usr/bin/env bash
set -o xtrace
set -e
mkdir -p $2/taxonomy
mkdir -p $2/library
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2/taxonomy
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2/taxonomy
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
# get host taxid from config
taxid=$(cat ../../../config/config.yaml | grep host_taxid | grep -oh -P "[0-9].*")
gunzip -c $1 > host.fasta
sed -i 's/>.*/>host/' host.fasta
echo -e "accession\taccession.version\ttaxid\tgi" > acc2taxid_host.txt
echo -e "host\t1\t$taxid\thost" >> acc2taxid_host.txt
/metacache/metacache build $2 host.fasta -taxonomy $2/taxonomy -taxpostmap acc2taxid_host.txt -kmerlen 32

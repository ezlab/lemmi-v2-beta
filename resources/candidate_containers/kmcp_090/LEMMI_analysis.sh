#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
pe=$7
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
if [ $pe -eq 1 ]
then
/kmcp search -d ../ref_host -1 ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -2 ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz -o host.tsv.gz
else
/kmcp search -d ../ref_host -1 ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -o host.tsv.gz
fi
/kmcp profile host.tsv.gz \
    --taxid-map        ../ref_host/taxid.map \
    --taxdump          ../ref_host/ \
    --out-prefix       host.tsv.gz.k.profile \
    --cami-report      host.tsv.gz.c.profile \
    --binning-result   host.tsv.gz.binning.gz
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz host.tsv.gz.binning.gz reads.r1.nohost.fastq
if [ $pe -eq 1 ]
then
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz host.tsv.gz.binning.gz reads.r2.nohost.fastq
fi
if [ $pe -eq 1 ]
then
/kmcp search -d ../ref_contaminants_$3 -1 reads.r1.nohost.fastq -2 reads.r2.nohost.fastq -o contaminants.tsv.gz
else
/kmcp search -d ../ref_contaminants_$3 -1 reads.r1.nohost.fastq -o contaminants.tsv.gz
fi
/kmcp profile contaminants.tsv.gz \
    --taxid-map        ../ref_contaminants_$3/taxid.map \
    --taxdump          ../ref_contaminants_$3/ \
    --out-prefix       contaminants.tsv.gz.k.profile \
    --cami-report      contaminants.tsv.gz.c.profile \
    --binning-result   contaminants.tsv.gz.binning.gz
filter_out.py reads.r1.nohost.fastq contaminants.tsv.gz.binning.gz reads.r1.noconta.fastq
if [ $pe -eq 1 ]
then
filter_out.py reads.r2.nohost.fastq contaminants.tsv.gz.binning.gz reads.r2.noconta.fastq
fi
if [ $pe -eq 1 ]
then
/kmcp search -d ../ref_targets_$3 -1 reads.r1.noconta.fastq -2 reads.r2.noconta.fastq -o targets.tsv.gz
else
/kmcp search -d ../ref_targets_$3 -1 reads.r1.noconta.fastq -o targets.tsv.gz
fi
/kmcp profile targets.tsv.gz \
    -m 1 \
    --taxid-map        ../ref_targets_$3/taxid.map \
    --taxdump          ../ref_targets_$3/ \
    --out-prefix       targets.tsv.gz.k.profile \
    --cami-report      targets.tsv.gz.c.profile \
    --binning-result   targets.tsv.gz.binning.gz
python3 /usr/bin/prepare_final_results.py targets.tsv.gz.k.profile $output_file $taxo

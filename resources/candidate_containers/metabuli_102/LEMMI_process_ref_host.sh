#!/usr/bin/env bash
set -o xtrace
set -e
db=$2
mkdir -p $db/taxonomy
tar zxvf ../../repository/taxdump.tar.gz -C $db/taxonomy/
edit_host.py $1 $3
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
edit_host.py $1
echo "host.fna" > list_host.txt
echo -e "host\thost.1\t9606\t1\nhost\thost.1\t9606\t1" > metabuli_host_accession2taxid.tsv
/metabuli/bin/metabuli build $db list_host.txt metabuli_host_accession2taxid.tsv --threads $cpus --taxonomy-path $db/taxonomy/

#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
gunzip -c ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz > reads.r1.fastq
gunzip -c ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz > reads.r2.fastq
/metabuli/bin/metabuli classify --threads $cpus reads.r1.fastq reads.r2.fastq ../ref_host outdirhost jobidhost
filter_out.py reads.r1.fastq outdirhost/jobidhost_classifications.tsv reads.r1.nohost.fastq
filter_out.py reads.r2.fastq outdirhost/jobidhost_classifications.tsv reads.r2.nohost.fastq
/metabuli/bin/metabuli classify --threads $cpus reads.r1.nohost.fastq reads.r2.nohost.fastq ../ref_contaminants_$instance outdirconta jobidconta
filter_out.py reads.r1.nohost.fastq outdirconta/jobidconta_classifications.tsv reads.r1.noconta.fastq
filter_out.py reads.r2.nohost.fastq outdirconta/jobidconta_classifications.tsv reads.r2.noconta.fastq
/metabuli/bin/metabuli classify --threads $cpus reads.r1.noconta.fastq reads.r2.noconta.fastq ../ref_targets_$instance outdirtargets jobidtargets
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
fix_report.py outdirtargets/jobidtargets_report.tsv report
cat report | cut -f3 > reads
cat report | cut -f5 > taxids
for i in $(cat taxids);do cat ../ref_targets_$instance/taxonomy/names.dmp | grep -P "^$i\t" |  grep scientific | cut -f3;done > organisms
echo -e "taxid\treads" > reads_abundance
paste taxids reads | sed 's/ \{1,\}/ /g' > reads_abundance.tmp
cat reads_abundance.tmp >> reads_abundance
tot_lines=$(cat reads.r1.noconta.fastq | wc -l)
tot_reads=$(echo "$tot_lines/4" | bc -l)
abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/taxonomy/references.tsv ../ref_targets_$instance/taxonomy/names.dmp $tot_reads abundance
paste organisms taxids reads abundance | sed 's/ \{2,\}//g' >> $output_file

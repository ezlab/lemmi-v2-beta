#!/usr/bin/python3
import sys
lines=[]
first=False
for line in open(sys.argv[1]):
    if first:
        lines.append(line.split('\t'))
    else:
        if line.split('\t')[3] in ['superkingdom' 'phylum', 'class', 'order', 'family', 'genus', 'species']:
            first=True
            lines.append(line.split('\t'))
for l in lines:
    if l[3] not in ['superkingdom' 'phylum', 'class', 'order', 'family', 'genus', 'species']:
        # assign the reads to the previous line that is not '-'
        j=1
        while lines[lines.index(l)-j][3] not in ['superkingdom' 'phylum', 'class', 'order', 'family', 'genus', 'species']:
            j+=1
        lines[lines.index(l)-j][2] = int(lines[lines.index(l)-j][2]) + int(l[2])
with open(sys.argv[2], 'w') as outp:
    for l in lines:
        if l[3] not in ['superkingdom' 'phylum', 'class', 'order', 'family', 'genus', 'species']:
            continue
        if l[2] == '0':
            continue
        outp.write('\t'.join([str(e) for e in l]))

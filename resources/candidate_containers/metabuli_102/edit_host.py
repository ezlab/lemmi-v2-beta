#!/usr/bin/env python3 
import sys, gzip
i=0
outp=open('host.fna', 'w')
for line in gzip.open(sys.argv[1], 'r'):
    line = line.decode('ascii')
    if line.startswith('>'):
        line = '>host.1\n'
        i+=1
    outp.write(line)

import sys
file_out = open(sys.argv[2], 'w')
file_out.write('#LEMMI-{}\n'.format(sys.argv[3]))
file_out.write('organism\ttaxid\treads\tabundance\n')
with open(sys.argv[1]) as file_in:
    for line in file_in:
        if line.split('\t')[0] =='ref':
            continue
        taxid=line.strip().split('\t')[12]
        organism=line.strip().split('\t')[14]
        reads=line.strip().split('\t')[7]
        abu=line.strip().split('\t')[1]
        file_out.write("{}\t{}\t{}\t{}\n".format(organism, taxid, reads, float(abu)/100))

file_out.close()

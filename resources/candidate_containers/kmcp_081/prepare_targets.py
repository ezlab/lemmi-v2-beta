#!/usr/bin/env python3 
import sys, gzip
open('taxid.map', 'w').close()
outp=open('targets_taxid.map', 'a')
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split()[0]
    outp.write('{}\t{}\n'.format(acc, taxid))
outp.close()

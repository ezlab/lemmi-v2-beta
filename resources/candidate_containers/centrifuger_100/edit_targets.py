#!/usr/bin/env python3 
import sys, gzip
open('centrifuger_targets_input.fasta', 'w').close()
conversion=open('conversion_targets.tsv', 'w')
conversion.write('accession\ttaxid\n')
conversion.close()
outp=open('centrifuger_targets_input.fasta', 'a')
outp2=open('conversion_targets.tsv', 'a')
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split('\t')[0]
    outp2.write('{}\t{}\n'.format(acc, taxid))
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>{}\n'.format(acc)
        outp.write(line)
outp.close()
outp2.close()

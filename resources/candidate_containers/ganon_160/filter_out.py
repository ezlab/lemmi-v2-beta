#!/usr/bin/env python3
import sys, gzip
read_id = set([])
outp = open(sys.argv[3], 'w')
for line in open(sys.argv[2]):
    read_id.add(line.split('\t')[0])
write = True
i=0
for line in open(sys.argv[1], 'r'):
    if i % 4 == 0:
        if line.strip()[1:].split('/')[0] in read_id:
            write = False
        else:
            write = True

    if write:
        outp.write(line)
    i+=1

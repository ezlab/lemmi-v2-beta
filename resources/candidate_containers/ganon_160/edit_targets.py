#!/usr/bin/env python3 
import sys, gzip
open('seqinfo_targets.txt', 'w').close()
outp=open('seqinfo_targets.txt', 'a')
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split('\t')[0]
    size=line.strip().split('\t')[-1]
    outp.write('{}.fna\t{}\t{}\n'.format(acc, acc, taxid))
    outp2=open('{}.fna'.format(acc), 'w')
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>{}\n'.format(acc)
        outp2.write(line)
outp.close()
outp2.close()

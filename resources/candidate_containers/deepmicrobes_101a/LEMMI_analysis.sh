#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
pe=$7
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
if [[ $pe -eq 1 ]]
then
    /DeepMicrobes/pipelines/tfrec_predict_kmer.sh -f ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -r ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz -t fastq -v /home/seppey/tokens_merged_12mers.txt -o reads -s 4000000 -k 12
else
    echo "do nothing for now"
fi
/DeepMicrobes/pipelines/predict_DeepMicrobes.sh -i reads.tfrec -b 8192 -l species -p 8 -m ../ref_targets_$instance -o predictions
report_profile.sh -i predictions.result.txt -o summarize.profile.txt -t 50 -l ../ref_targets_demo_prok_gtdb/taxo.txt
echo -e "name\treads" > reads.tsv
cat summarize.profile.txt >> reads.tsv
python3 abundance_reads_to_genomes_gtdb.py reads.tsv ../ref_targets_demo_prok_gtdb/references.tsv ../ref_targets_demo_prok_gtdb/names.dmp $output_file

#!/usr/bin/env bash
export PATH=$PATH:/root/mambaforge/bin/
set -o xtrace
set -e
db=$2
export PATH=$PATH:/root/mambaforge/bin/
python3 /usr/bin/edit_targets.py $1 $3
mkdir -p $db
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mv list.txt $2
cd $2
for g in $(cut -f1 list.txt);do gunzip -c ../../../repository/genomes/$g.gz > $g;done
fna_label.py -m list.txt
for i in label*.fna;do python3 /readSimulator/readSimulator.py --input $i --simulator art --simulator_path /art/art_illumina --outdir $(pwd) --readlen 250 --depth 1 --opts '-m 400 -s 25 -qU 35 -nf 0 -rs 10 -ss MSv3 -qs -1.5 -qs2 -2 -na';done

for i in label_GCA_*_1.fastq.gz;do gunzip -c $i;done > all.fq
for i in label_GCA_*_2.fastq.gz;do gunzip -c $i >> all.fq;done

random_trim.py -i all.fq -o output_fasta -f fastq -l 250 -min 0 -max 150

/DeepMicrobes/pipelines/tfrec_train_kmer.sh -i output_fasta -v /home/seppey/tokens_merged_12mers.txt -o train.tfrec -s 204800 -k 12

/DeepMicrobes/DeepMicrobes.py --input_tfrec=train.tfrec --model_name=attention --model_dir=. --num_classes=$(cat list.txt | wc -l) -lr 0.001 > out_train 2>&1

cp $1 $2/references.tsv

if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi


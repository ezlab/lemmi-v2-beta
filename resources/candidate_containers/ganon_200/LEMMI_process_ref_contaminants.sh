#!/usr/bin/env bash
set -o xtrace
set -e
mkdir -p $2
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
edit_contaminants.py $1 $3
ganon build-custom --db-prefix $2/ref_contaminants \
            --input-file seqinfo_contaminants.tsv \
            --taxonomy-files $2/nodes.dmp $2/names.dmp \
            --threads $cpus \
            --taxonomy ncbi \
            --skip-genome-size \
            --filter-type ibf

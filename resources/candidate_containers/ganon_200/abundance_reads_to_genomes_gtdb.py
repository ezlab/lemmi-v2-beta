#!/mamba/bin/python3.10
"""
Call this companion script within your docker to obtain the corresponding "genome copy" abundance to your read count

as follows:
python3 abundance_reads_to_genomes.py [reads.tsv] [reference_list] [names.dmp] [output file]

where
reads.tsv is a file containing two columns, either taxid or name, and reads.
The script will detect what to use based on the header field "taxid" or "name"

taxid	reads
562	876

or

name	reads
Escherichia coli	876

or

name	reads
o__Acetobacterales	755

reference_list is the first parameter that is passed to LEMMI_process_ref_targets.sh

names.dmp is the file corresponding to the taxonomy that you used, that is made available to the container

output file will contain 1 column, without header, with the normalized value in the order of the original file

you need to install the pandas package within the container

Do not provide reads counted more than once, for instance at species and genus levels

"""

import sys

import pandas as pd
import numpy as np

predictions_in_reads_file = sys.argv[1]
ref_genomes_file = sys.argv[2]
names_dmp_file = sys.argv[3]

predictions_in_reads = pd.read_csv(predictions_in_reads_file, header=0, sep="\t")
if len(predictions_in_reads) == 0:
    open(sys.argv[-1], 'w').close()
    exit(0)
ref_genomes = pd.read_csv(ref_genomes_file, header=1, sep="\t")
# if the header is taxid and not name, need to load the names.dmp file
if predictions_in_reads.columns[0] == 'taxid':
    names_dmp = pd.read_csv(names_dmp_file, header=None, usecols=[0, 2], names=['taxid', 'name'], sep="\t")
    predictions_in_reads = predictions_in_reads.merge(names_dmp, on="taxid")


# for each line of result
def match_lineage(name):
    ref_genomes['belongs_to_lineage'] = ref_genomes.apply(
        lambda row: name in row['gtdb_taxonomy'].split(';') or name in row['__gtdb_accession'], axis=1)
    return ref_genomes[ref_genomes['belongs_to_lineage']].__size.mean()


predictions_in_reads['mean_size'] = predictions_in_reads.apply(lambda row: match_lineage(row['name']), axis=1)
total_size = predictions_in_reads['mean_size'].sum()

predictions_in_reads['normalized_abundance_tmp'] = predictions_in_reads.apply(lambda row: (
            row['reads'] / (predictions_in_reads['reads'].max()) * (
                predictions_in_reads['mean_size'].mean() / row['mean_size'])), axis=1)
predictions_in_reads['normalized_abundance'] = predictions_in_reads.apply(
    lambda row: row['normalized_abundance_tmp'] / (predictions_in_reads['normalized_abundance_tmp'].sum()), axis=1)

predictions_in_reads.replace(np.nan, 0, inplace=True)
predictions_in_reads['normalized_abundance'].to_csv(sys.argv[-1], header=False, index=False)

#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
ganon classify --db-prefix ../ref_targets_$3/ref_targets --paired-reads  ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz -o targets --threads $cpus || true
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
touch targets.tre
cat targets.tre | grep -v "unclassified\|root" | cut -f4 > organisms
cat targets.tre | grep -v "unclassified\|root" | cut -f8 > reads
cat targets.tre | grep -v "unclassified\|root" | cut -f2 > taxids
cat targets.tre | grep -v "unclassified\|root" | cut -f9 > perc
for i in $(cat perc);do echo "$i*0.01" | bc;done > abundance
paste organisms taxids reads abundance | sed 's/ \{2,\}//g' >> $output_file

#!/usr/bin/env python3 
import sys, gzip
open('seqinfo_targets.tsv', 'w').close()
open('sizes.tsv', 'w').close()
outp=open('seqinfo_targets.tsv', 'a')
outp3=open('sizes.tsv', 'a')
outp3.write("#species_taxid\tmin_ungapped_length\tmax_ungapped_length\texpected_ungapped_length\tnumber_of_genomes\tmethod_determined\n")
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split('\t')[0]
    size=line.strip().split('\t')[-1]
    outp.write('{}\t{}\t{}\n'.format("{}.fna".format(acc), acc, taxid))
    outp3.write('{}\t{}\t{}\t{}\t{}\t{}\n'.format(taxid, size, size, size, 1, "lemmi"))
    outp2=open("{}.fna".format(acc), "w")
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>{}\n'.format(acc)
        outp2.write(line)
    outp2.close()
outp.close()
outp3.close()

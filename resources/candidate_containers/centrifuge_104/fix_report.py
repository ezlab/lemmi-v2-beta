#!/usr/bin/python3
import sys
lines=[]
for line in open(sys.argv[1]):
    if line.split()[5] not in ['root', 'unclassified']:
        lines.append(line.split('\t'))
for l in lines:
    if l[3] == '-':
        # assign the reads to the previous line that is not '-'
        j=1
        while lines[lines.index(l)-j][3] == '-':
            j+=1
        lines[lines.index(l)-j][2] = int(lines[lines.index(l)-j][2]) + int(l[2])
with open(sys.argv[2], 'w') as outp:
    for l in lines:
        if l[3] == '-':
            continue
        if l[2] == '0':
            continue
        outp.write('\t'.join([str(e) for e in l]))

#!/usr/bin/env bash
set -o xtrace
set -e
edit_contaminants.py $1 $3
mkdir -p $2/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2/taxonomy
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2/taxonomy/
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi

centrifuge-build -p $cpus --conversion-table conversion_contaminants.tsv --taxonomy-tree $2/taxonomy/nodes.dmp --name-table $2/taxonomy/names.dmp centrifuge_contaminants_input.fasta $2

#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
pe=$6

mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
if [ $pe -eq 1 ]
then
centrifuge -p $cpus -x ../ref_host -1 ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -2 ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz --report-file host.report -S host
else
centrifuge  --min-hitlen 500 -p $cpus -x ../ref_host -U ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz --report-file host.report -S host
fi
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz host reads.r1.nohost.fastq
if [ $pe -eq 1 ]
then
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz host reads.r2.nohost.fastq
fi

if [ $pe -eq 1 ]
then
centrifuge -p $cpus -x ../ref_contaminants_$instance -1 reads.r1.nohost.fastq -2 reads.r2.nohost.fastq --report-file conta.report -S conta
else
centrifuge  --min-hitlen 500 -p $cpus -x ../ref_contaminants_$instance -U reads.r1.nohost.fastq -S conta
fi
filter_out.py reads.r1.nohost.fastq conta reads.r1.noconta.fastq
if [ $pe -eq 1 ]
then
filter_out.py reads.r2.nohost.fastq conta reads.r2.noconta.fastq
fi


cp reads.r1.nohost.fastq reads.r1.noconta.fastq
cp reads.r2.nohost.fastq reads.r2.noconta.fastq

if [ $pe -eq 1 ]
then
centrifuge --verbose -p $cpus -x ../ref_targets_$instance -1 reads.r1.noconta.fastq -2 reads.r2.noconta.fastq --report-file targets.report -S targets
else
centrifuge --min-hitlen 500 --verbose -p $cpus -x ../ref_targets_$instance -U reads.r1.noconta.fastq --report-file targets.report -S targets
fi

centrifuge-kreport -x ../ref_targets_$instance targets > kreport

fix_report.py kreport report

echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
cat report | cut -f3 > reads
cat report | cut -f5 > taxids
for i in $(cat taxids);do cat ../ref_targets_$instance/taxonomy/names.dmp | grep -P "^$i\t" |  grep scientific | cut -f3;done > organisms
echo -e "taxid\treads" > reads_abundance
paste taxids reads | sed 's/ \{1,\}/ /g' > reads_abundance.tmp
cat reads_abundance.tmp >> reads_abundance
tot_lines=$(cat reads.r1.noconta.fastq | wc -l)
tot_reads=$(echo "$tot_lines/4" | bc -l)
abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/references.tsv ../ref_targets_$instance/taxonomy/names.dmp $tot_reads abundance
paste organisms taxids reads abundance | sed 's/ \{2,\}//g' >> $output_file

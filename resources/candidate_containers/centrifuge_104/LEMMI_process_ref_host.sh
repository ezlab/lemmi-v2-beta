mkdir -p $2/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2/taxonomy
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2/taxonomy
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
# get host taxid from config
taxid=$(cat ../../../config/config.yaml | grep host_taxid | grep -oh -P "[0-9].*")
gunzip -c $1 > host.fasta
sed -i 's/>.*/>host/' host.fasta
echo -e "accession\ttaxid" > conversion_host.tsv
echo -e "host\t$taxid" >> conversion_host.tsv

centrifuge-build -p $cpus --conversion-table conversion_host.tsv --taxonomy-tree $2/taxonomy/nodes.dmp --name-table $2/taxonomy/names.dmp host.fasta $2

#!/usr/bin/env bash
set -o xtrace
set -e
mtsv_idx=$2
edit_host.py $1
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir -p $mtsv_idx
index=`basename   $mtsv_idx`
GBS_PER_CHUNK=1
echo "index_host: $index" >> config.yaml
echo "GBS_PER_CHUNK_host: $GBS_PER_CHUNK" >> config.yaml
echo "cpus: $cpus" >> config.yaml
cp /app/Snakefile*.aba .
snakemake -p --cores 1 -s Snakefile1.aba
snakemake -p --cores 1 -s Snakefile2.aba
#sleep 20

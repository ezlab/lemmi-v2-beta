#!/usr/bin/env python3 
import sys, gzip
i=0
outp=open('mtsv_host_input.fasta', 'w')
for line in gzip.open(sys.argv[1], 'r'):
    line = line.decode('ascii')
    if line.startswith('>'):
        line = '>%s-9606\n' % i
        i+=1
    outp.write(line)

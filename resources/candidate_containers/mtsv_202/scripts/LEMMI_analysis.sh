#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
cp  ../config.yaml  configAnalysis.yaml

if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
echo "index_join: ../../../instances/$instance/$sample_folder" >> configAnalysis.yaml
cp ../Snakefile0.aba .
snakemake --cores $cpus -s Snakefile0.aba reads.rjoin.fasta
#joinReadsgz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz \
#              ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz \
#              reads.rjoin.fasta

#Host
cp ../Snakefile3.aba .
snakemake --cores 1 -s Snakefile3.aba  mtsv_host_result.result
awk -F ":" '{print $1}' mtsv_host_result.result >  host.out
selectFasta -list host.out -fasta reads.rjoin.fasta  > reads.nohost.fasta || true

#CONT
cp ../Snakefile6.aba .
snakemake --cores 1 -s Snakefile6.aba  mtsv_cont_result.result
awk -F ":" '{print $1}' mtsv_cont_result.result >  conta.out
selectFasta -list conta.out -fasta reads.nohost.fasta  > reads.noconta.fasta || true

#TARGET
cp ../Snakefile9.aba .
snakemake --cores 1 -s Snakefile9.aba  mtsv_targ_result.result
awk -F ":" '{print $1}' mtsv_targ_result.result >  targets.out

#OUT
echo -e "taxid\treads" > reads_abundance
awk -F ":" '{print $2}' mtsv_targ_result.result | awk -F ","  '{print $1}' | sort -g  \
     | uniq -c | awk '{print $2"\t"$1}' >>  reads_abundance
abundance_reads_to_genomes_${taxo}.py reads_abundance ../targets_ref.$instance.tsv \
                          ../ref_targets_$instance/taxonomy/names.dmp abundance

tail -n +2 reads_abundance | awk '{print $1}' > taxids
tail -n +2 reads_abundance | awk '{print $2}' > reads
grep -Fw -f taxids ../ref_targets_$instance/taxonomy/names.dmp | awk -F '|' '{print $2}' | \
sed 's/^[ \t]*//' | awk '{print $1}'> organisms

echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
paste organisms taxids reads abundance >> $output_file

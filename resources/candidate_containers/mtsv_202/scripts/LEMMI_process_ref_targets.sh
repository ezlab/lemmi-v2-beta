#!/usr/bin/env bash
set -o xtrace
set -e
set -o xtrace
set -e
mtsvdb=$2
edit_targets.py $1 $3
mkdir -p $mtsvdb/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $mtsvdb/taxonomy/
else
    tar zxvf ../../repository/taxdump.tar.gz -C $mtsvdb/taxonomy/
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
index=`basename   $mtsvdb`
GBS_PER_CHUNK=1
echo "index_targ: $index" >> config.yaml
echo "GBS_PER_CHUNK_targ: $GBS_PER_CHUNK" >> config.yaml
echo "cpus: $cpus" >> config.yaml
cp /app/Snakefile*.aba .
snakemake -p --cores 1 -s Snakefile7.aba
snakemake -p --cores 1 -s Snakefile8.aba



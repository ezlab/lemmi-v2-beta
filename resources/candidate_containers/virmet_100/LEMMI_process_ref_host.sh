#!/usr/bin/env bash
set -o xtrace
mkdir -p human/fasta
cp $1 human/fasta/GRCh38.fasta.gz
virmet index --human
mkdir -p $2
mv human $2

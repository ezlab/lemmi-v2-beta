#!/usr/bin/env python3 
import sys, gzip
open('viral_nuccore/viruses.fasta', 'w').close()
outp=open('viral_nuccore/viruses.fasta', 'a')
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split('\t')[0]
    name=line.split('\t')[2].split(';')[-1]
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>something {}\n'.format(name)
        outp.write(line)
outp.close()

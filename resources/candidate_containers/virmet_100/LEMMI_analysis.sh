#!/usr/bin/env bash
set -o xtrace
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
mkdir -p ./virmet_databases
mkdir -p virmet_output_reads/viral_nuccore
cp ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz .
cp -r ../ref_contaminants_$instance/bacteria ./virmet_databases/
cp -r ../ref_contaminants_$instance/fungi ./virmet_databases/
cp -r ../ref_contaminants_$instance/bovine ./virmet_databases/
cp -r ../ref_host/human ./virmet_databases/
cp -r ../ref_targets_$instance/viral_nuccore/* virmet_output_reads/viral_nuccore/
ln -s /data/virmet_databases $(pwd)/virmet_databases/
virmet wolfpack --file reads.r1.fastq.gz
tail -n+2 virmet_output_reads/orgs_list.tsv | cut -f1 | cut -f1 -d"|" > organisms
tail -n+2 virmet_output_reads/orgs_list.tsv | cut -f2 > reads
echo -e "name\treads" > reads_abundance
paste organisms reads >> reads_abundance
python3.6 /usr/bin/abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/references.tsv ../ref_targets_$instance/taxonomy/names.dmp abundance
echo -e "#LEMMI_$5\norganism\treads\tabundance" > $output_file
paste organisms reads abundance >> $output_file

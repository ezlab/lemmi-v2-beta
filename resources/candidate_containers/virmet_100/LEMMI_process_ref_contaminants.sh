#!/usr/bin/env bash
set -o xtrace
mkdir -p bovine/fasta
mkdir -p fungi/fasta/
mkdir -p bacteria/fasta/
# recreate the expected structure so it works, although not reflecting the content
rm bacteria/fasta/bact1.fasta.gz || true
for g in $(cut -f1 $1 | grep -v ncbi_genbank_assembly_accession);do cat ../../repository/genomes/$g.fna.gz >> bacteria/fasta/bact1.fasta.gz;done
gunzip -c bacteria/fasta/bact1.fasta.gz | head -n 1000 > bacteria/fasta/bact2.fasta && gzip bacteria/fasta/bact2.fasta
gunzip -c bacteria/fasta/bact1.fasta.gz | head -n 1000 > bacteria/fasta/bact3.fasta && gzip bacteria/fasta/bact3.fasta
gunzip -c bacteria/fasta/bact1.fasta.gz | head -n 1000 > fungi/fasta/fungi1.fasta && gzip fungi/fasta/fungi1.fasta
gunzip -c bacteria/fasta/bact1.fasta.gz | head -n 1000 > bovine/fasta/bt_ref_Bos_taurus_UMD_3.1.1.fasta && gzip bovine/fasta/bt_ref_Bos_taurus_UMD_3.1.1.fasta
virmet index --bact
virmet index --bovine
virmet index --fungal
mkdir $2
mv bacteria $2
mv fungi $2
mv bovine $2


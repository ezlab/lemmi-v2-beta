#!/usr/bin/env python3
import sys
inp1 = sys.argv[1]
gi=1
outp1 = open('viral_nuccore/viral_database.fasta','w')
for line in open(inp1):
    if line.startswith('>'):
        line = '>gi|{}|ref|{}|xxx\n'.format(str(gi),line.strip()[1:])
        gi+=1
    outp1.write(line)
inp2 = sys.argv[2]
outp2 = open('viral_nuccore/viral_gi_taxid.dmp','w')
gi=1
for line in open(inp2):
    if 'ncbi_genbank_assembly_accession' not in line and not line.startswith('#'):
        outp2.write('{}\t{}'.format(str(gi),line.split('\t')[1]))
        gi+=1

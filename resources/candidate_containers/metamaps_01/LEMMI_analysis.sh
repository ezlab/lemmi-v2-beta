#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
pe=$7
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
unset MALLOC_ARENA_MAX
/MetaMaps/metamaps mapDirectly -t $cpus --all -r ../ref_host/host/DB.fa -q ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz -o host
/MetaMaps/metamaps classify -t $cpus --mappings host --DB ../ref_host/host/ || true
filter_out_gz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz host.EM.reads2Taxon reads.r1.nohost.fastq || cp ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz reads.r1.nohost.fastq

/MetaMaps/metamaps mapDirectly -t $cpus --all -r ../ref_contaminants_$instance/targets/DB.fa -q reads.r1.nohost.fastq -o conta
/MetaMaps/metamaps classify -t $cpus --mappings conta --DB  ../ref_contaminants_$instance/targets/ || true
filter_out.py reads.r1.nohost.fastq conta.EM.reads2Taxon reads.r1.noconta.fastq || cp reads.r1.nohost.fastq reads.r1.noconta.fastq

/MetaMaps/metamaps mapDirectly -t $cpus --all -r ../ref_targets_$instance/targets/DB.fa -q reads.r1.noconta.fastq -o targets
/MetaMaps/metamaps classify -t $cpus --mappings targets --DB ../ref_targets_$instance/targets/ || true

touch targets.EM.WIMP

cat targets.EM.WIMP | grep -v "Unclassified\|definedGenomes\|totalReads\|readsLongEnough\|readsLongEnough_unmapped\|AnalysisLevel" | cut -f2 > taxids
cat targets.EM.WIMP | grep -v "Unclassified\|definedGenomes\|totalReads\|readsLongEnough\|readsLongEnough_unmapped\|AnalysisLevel" | cut -f3 > organisms
cat targets.EM.WIMP | grep -v "Unclassified\|definedGenomes\|totalReads\|readsLongEnough\|readsLongEnough_unmapped\|AnalysisLevel" | cut -f4 > reads
cat targets.EM.WIMP | grep -v "Unclassified\|definedGenomes\|totalReads\|readsLongEnough\|readsLongEnough_unmapped\|AnalysisLevel" | cut -f5 > abu

echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
paste organisms taxids reads abu >> $output_file

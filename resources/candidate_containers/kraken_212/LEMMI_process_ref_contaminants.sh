#!/usr/bin/env bash
set -o xtrace
set -e
krakendb=$2
edit_contaminants.py $1 $3
mkdir -p $krakendb/taxonomy
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $krakendb/taxonomy/
else
    tar zxvf ../../repository/taxdump.tar.gz -C $krakendb/taxonomy/
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
kraken2-build --threads $cpus --add-to-library kraken_contaminants_input.fasta --db $krakendb
kraken2-build --threads $cpus --build --db $krakendb

#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
/kraken2/kraken2 --confidence 0.51 --threads $cpus --paired ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz --db ../ref_targets_$instance --output targets.out --report target.report
alias python=python3
/bracken2/bracken -d ../ref_targets_$instance -i  target.report -o target.bracken -r 150 || touch target_bracken_species.report
echo -e "#LEMMI_$5\norganism\ttaxid\treads\tabundance" > $output_file
cat target_bracken_species.report | grep -v "unclassified\|root" | cut -f3 > reads
cat target_bracken_species.report | grep -v "unclassified\|root" | cut -f6 > organisms
cat target_bracken_species.report | grep -v "unclassified\|root" | cut -f5 > taxids
echo -e "taxid\treads" > reads_abundance
paste taxids reads | sed 's/ \{1,\}/ /g' >> reads_abundance
tot_lines=$(gunzip -c ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz | wc -l)
tot_reads=$(echo "$tot_lines/4" | bc -l)
abundance_reads_to_genomes_${taxo}.py reads_abundance ../ref_targets_$instance/taxonomy/references.tsv ../ref_targets_$instance/taxonomy/names.dmp $tot_reads abundance
paste organisms taxids reads abundance | sed 's/ \{2,\}//g' >> $output_file

#!/usr/bin/env python3 
import sys, gzip
outp=open(sys.argv[1])
i=0
outp=open('kraken_host_input.fasta', 'w')
for line in gzip.open(sys.argv[1], 'r'):
    line = line.decode('ascii')
    if line.startswith('>'):
        line = '>benchmark_host_%s|kraken:taxid|9606\n' % i
        i+=1
    outp.write(line)

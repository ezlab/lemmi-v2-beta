#!/usr/bin/env python3
import sys, gzip
outp=open('smash_targets_input_list.txt','w').close()
outp=open('smash_targets_input_list.txt','a')
#genomesPath=sys.argv[3]
genomesPath="../../repository/genomes/"
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split()[0]
    genome=genomesPath+acc+".fna.gz"
    outp.write(genome+'\n')
outp.close()

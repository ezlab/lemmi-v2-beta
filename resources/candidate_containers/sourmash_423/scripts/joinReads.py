#!/usr/bin/env python3 
import sys, gzip
import os

outp=open(sys.argv[3], 'w')
i=0
reads={}
for line in open(sys.argv[1], 'r'):	
	if i % 4 == 0:
		name=line.strip()[1:].split('/')[0]
		reads[name]=line
	else:
		reads[name]+=line
	i+=1
	
i=0
for line in open(sys.argv[2], 'r'):	
	if i % 4 == 0:
		name=line.strip()[1:].split('/')[0]
	reads[name]+=line
	i+=1		
		
for items in reads.keys():
	outp.write(reads[items])	
	
totalReads=open(sys.argv[4], 'w')
totalReads.write(str(len(reads.keys())))

#!/usr/bin/env python3 
import sys, gzip
open('kraken_contaminants_input.fasta', 'w').close()
outp=open('kraken_contaminants_input.fasta', 'a')
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split()[0]
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>{}|kraken:taxid|{}\n'.format(acc, taxid)
        outp.write(line)
outp.close()

#!/usr/bin/env bash
set -o xtrace
set -e
sample_folder=$1
output_file=$2
instance=$3
taxo=$5
mkdir -p tmp_$sample_folder
cd tmp_$sample_folder
echo "intersect_bp" >  targets.out
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
joinReadsgz.py ../../../instances/$instance/$sample_folder/reads.r1.fastq.gz ../../../instances/$instance/$sample_folder/reads.r2.fastq.gz reads.rjoin.fastq totalReads.txt
totalReads=`cat totalReads.txt`
sourmash sketch dna reads.rjoin.fastq -p abund -o reads.target.sig
sourmash gather reads.target.sig  ../ref_targets_$instance/smash_targets_input.sbt.zip -o targets.out > targets.txt
processOut.py targets.txt targets.out ../ref_targets_$instance/taxonomy/references.tsv $taxo $totalReads targets.predictions
#echo -e "#LEMMI16s\norganism\ttaxid\treads\tabundance" > $output_file
cp targets.predictions $output_file

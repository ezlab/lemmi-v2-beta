#!/usr/bin/env bash
set -o xtrace
set -e
mkdir -p $2
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
edit_contaminants.py $1 $3
ganon build --db-prefix $2/ref_contaminants \
            --input-files ganon_contaminants_input.fasta \
            --taxdump-file $2/nodes.dmp $2/names.dmp \
            --seq-info-file seqinfo_contaminants.txt --threads $cpus

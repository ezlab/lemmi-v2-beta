#!/usr/bin/env python3 
import sys, gzip
open('ganon_contaminants_input.fasta', 'w').close()
open('seqinfo_contaminants.txt', 'w').close()
outp=open('ganon_contaminants_input.fasta', 'a')
outp2=open('seqinfo_contaminants.txt', 'a')
for line in open(sys.argv[1]):
    if line.startswith('ncbi_genbank_assembly_accession') or line.startswith('#'):
        continue
    if sys.argv[2] == 'gtdb':
        taxid=line.split('\t')[4]
    else:
        taxid=line.split('\t')[1]
    acc=line.split('\t')[0]
    size=line.strip().split('\t')[-1]
    outp2.write('{}\t{}\t{}\t{}\n'.format(acc, size, taxid, taxid))
    for line in gzip.open("../../repository/genomes/{}.fna.gz".format(acc), 'rb'):
        line = line.decode('ascii')
        if line.startswith('>'):
            line = '>{}\n'.format(acc)
        outp.write(line)
outp.close()
outp2.close()

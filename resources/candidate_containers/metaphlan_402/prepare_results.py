#!/usr/bin/env python3
import sys
outp=open(sys.argv[2], 'a')
for line in open(sys.argv[1]):
    if line.startswith('@') or line.startswith('#'):
        continue
    line = line.strip().split('\t')
    if line == [] or line == ['']:
        continue
    outp.write('{}\t{}\t{}\t{}\n'.format(line[0].split('|')[-1].split('__')[-1].replace('_',' '), line[1], 1000*float(line[2])/100, float(line[2])/100))
outp.close()

#!/usr/bin/env bash
set -o xtrace
set -e
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir -p $2
tar zxvf ../../repository/taxdump.tar.gz -C $2
taxid=$(cat ../../../config/config.yaml | grep host_taxid | grep -oh -P "[0-9].*")
echo -e "host\t"$taxid > host_taxidmapping
gunzip -c $1 > host.fna
sed -i 's/>.*/>host/' host.fna
export MMSEQS_NUM_THREADS=$cpus
mmseqs createdb host.fna $2/host
mmseqs createtaxdb $2/host host_tmp --ncbi-tax-dump $2 --tax-mapping-file host_taxidmapping
mmseqs createindex $2/host host_tmp

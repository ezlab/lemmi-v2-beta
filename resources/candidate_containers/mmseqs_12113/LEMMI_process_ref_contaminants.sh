#!/usr/bin/env bash
set -o xtrace
set -e
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir -p $2
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi
edit_contaminants.py $1 $3
export MMSEQS_NUM_THREADS=$cpus
mmseqs createdb mmseq2_contaminants_input.fasta $2/contaminants
mmseqs createtaxdb $2/contaminants conta_tmp --ncbi-tax-dump $2 --tax-mapping-file conta_taxidmapping
mmseqs createindex $2/contaminants conta_tmp

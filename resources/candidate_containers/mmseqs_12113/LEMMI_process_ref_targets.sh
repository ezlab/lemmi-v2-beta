#!/usr/bin/env bash
set -o xtrace
set -e
if [ -z ${cpus+x} ]; then cpus=$(nproc);fi
mkdir -p $2
if [ $3 = 'gtdb' ]; then
    tar zxvf ../../repository/gtdb_taxdump.tar.gz -C $2
else
    tar zxvf ../../repository/taxdump.tar.gz -C $2
fi
edit_targets.py $1 $3
export MMSEQS_NUM_THREADS=$cpus
cp $1 $2/references.tsv
mmseqs createdb mmseq2_targets_input.fasta $2/targets
mmseqs createtaxdb $2/targets targets_tmp --ncbi-tax-dump $2 --tax-mapping-file targets_taxidmapping
mmseqs createindex $2/targets targets_tmp
